#include<bits/stdc++.h>
using namespace std;

int max(int,int);
void print(string &,string &,int,int);

int main()
{
	string s1,s2;
	getline(cin,s1);
	getline(cin,s2);
	int length1=s1.length();
	int length2=s2.length();
	print(s1,s2,length1,length2);
	return 0;
}
void print(string &s1,string &s2,int l1,int l2)
{
	int list[l1+1][l2+1];
	for(int i=0;i<=l1;i++)
	{
		for(int j=0;j<=l2;j++)
		{
			if(i==0 || j==0)//initialize first row and first column to zero
				list[i][j]=0;
			else if(s1[i]==s2[j])
				list[i][j]=1+list[i-1][j-1];//increment the diagonal value
			else
				list[i][j]=max(list[i-1][j],list[i][j-1]);//maximum of left and top element
		}
	}
	cout<<list[l1][l2];//which contains the value of longest common subsequence
}
int max(int a,int b)
{
return a>b?a:b;
}
