#include<bits/stdc++.h>
using namespace std;
struct cache
{
	int valid;
	char * tag;
	char * block;
};

void hex2bin(char input[], char output[]);
int sumofindex(char a[],int index);

int main()
{
	int n,hit=0,miss=0;
	int blocksize=32;
	int cachesize=4096;
	int setnum=cachesize/blocksize;//128
	int tag=20;
	int index=7;
	int offset=5;
	cout<<"enter number of addresses"<<endl;
	cin>>n;
	struct cache mycache[setnum];
	for(int i=0;i<setnum;i++)//initilaize the cache
	{
		mycache[i].valid=0;
		mycache[i].tag=(char *)malloc(sizeof(char)*tag);
		for(int j=0;j<tag;j++)
			mycache[i].tag[j]=0;
		mycache[i].block=(char *)malloc(32);
	}
	for(int i=0;i<n;i++)
	{
		char a1[8]={'0'};
		char a[8]={'0'};
		int size=8;
		cout<<"enter address"<<endl;
		cin>>a1;
		int length=strlen(a1);
		if(strlen(a1)<8)//if the address less than 8 add preceding zero
		{
			int zero_needed=size-(int)strlen(a1);//number of zeros to insert
			for(int m=0;m<zero_needed;m++)
			{
				a[m]='0';			
			}		
			for(int m=0;m<strlen(a1);m++)
			{
				a[m+zero_needed]=a1[m];
			}
		}
		a[8]='\0';	
		char b[32];
		hex2bin(a,b);
		char tagbits[tag];
		for(int k=0;k<20;k++)//collect tag-bits
			tagbits[k]=b[k];

		char indexbits[index];
		for(int k=0;k<index;k++)//collect index-bits
			indexbits[k]=b[k+20];

		int calculatedindex=sumofindex(indexbits,index);//calculate decimal value of bits which act as index for calculating set number

		if(mycache[calculatedindex].valid==0)//compulsory miss
		{
			mycache[calculatedindex].valid=1;
			strcpy(mycache[calculatedindex].tag,indexbits);
			miss++;
		}
		else
		{
			if(strcmp(mycache[calculatedindex].tag,indexbits)==0)//if the block is present in the cache
			{
				hit++;
			}
			else//copy the block into cache
			{
			strcpy(mycache[calculatedindex].tag,indexbits);
			miss++;
			}
		}
	
		
	}
	cout<<"miss is"<<miss<<endl;
	cout<<"hit is"<<hit<<endl;
	return 0;

}
//find the decimal value of binary array
int sumofindex(char a[],int index)
{
	int sum=0;
	for(int i=0;i<index;i++)
	{
		int val;
		if(a[i]=='0')
			val=0;
		else
			val=1;
		sum=sum+(val*pow(2,index-i-1));
	}
	return sum;
		
}

//convert hex address into binary address
void hex2bin(char input[], char output[])
{
    int i;
    int a = 0;
    int b = 1;
    int c = 2;
    int d = 3;
    int x = 4;
    int size;
    size = strlen(input);

    for (i = 0; i < size; i++)
    {
        if (input[i] =='0')
        {
            output[i*x +a] = '0';
            output[i*x +b] = '0';
            output[i*x +c] = '0';
            output[i*x +d] = '0';
        }
        else if (input[i] =='1')
        {
            output[i*x +a] = '0';
            output[i*x +b] = '0';
            output[i*x +c] = '0';
            output[i*x +d] = '1';
        }    
        else if (input[i] =='2')
        {
            output[i*x +a] = '0';
            output[i*x +b] = '0';
            output[i*x +c] = '1';
            output[i*x +d] = '0';
        }    
        else if (input[i] =='3')
        {
            output[i*x +a] = '0';
            output[i*x +b] = '0';
            output[i*x +c] = '1';
            output[i*x +d] = '1';
        }    
        else if (input[i] =='x')
        {
            output[i*x +a] = '0';
            output[i*x +b] = '1';
            output[i*x +c] = '0';
            output[i*x +d] = '0';
        }    
        else if (input[i] =='5')
        {
            output[i*x +a] = '0';
            output[i*x +b] = '1';
            output[i*x +c] = '0';
            output[i*x +d] = '1';
        }    
        else if (input[i] =='6')
        {
            output[i*x +a] = '0';
            output[i*x +b] = '1';
            output[i*x +c] = '1';
            output[i*x +d] = '0';
        }    
        else if (input[i] =='7')
        {
            output[i*x +a] = '0';
            output[i*x +b] = '1';
            output[i*x +c] = '1';
            output[i*x +d] = '1';
        }    
        else if (input[i] =='8')
        {
            output[i*x +a] = '1';
            output[i*x +b] = '0';
            output[i*x +c] = '0';
            output[i*x +d] = '0';
        }
        else if (input[i] =='9')
        {
            output[i*x +a] = '1';
            output[i*x +b] = '0';
            output[i*x +c] = '0';
            output[i*x +d] = '1';
        }
        else if (input[i] =='a')
        {    
            output[i*x +a] = '1';
            output[i*x +b] = '0';
            output[i*x +c] = '1';
            output[i*x +d] = '0';
        }
        else if (input[i] =='b')
        {
            output[i*x +a] = '1';
            output[i*x +b] = '0';
            output[i*x +c] = '1';
            output[i*x +d] = '1';
        }
        else if (input[i] =='c')
        {
            output[i*x +a] = '1';
            output[i*x +b] = '1';
            output[i*x +c] = '0';
            output[i*x +d] = '0';
        }
        else if (input[i] =='d')
        {    
            output[i*x +a] = '1';
            output[i*x +b] = '1';
            output[i*x +c] = '0';
            output[i*x +d] = '1';
        }
        else if (input[i] =='e')
        {    
            output[i*x +a] = '1';
            output[i*x +b] = '1';
            output[i*x +c] = '1';
            output[i*x +d] = '0';
        }
        else if (input[i] =='f')
        {
            output[i*x +a] = '1';
            output[i*x +b] = '1';
            output[i*x +c] = '1';
            output[i*x +d] = '1';
        }
    }

    output[32] = '\0';
}


