#include<bits/stdc++.h>
using namespace std;
struct node
{
int data;
struct node * prev;
struct node * next;
};

struct node * create(int data);
void insert(int data);
void print(struct node * head);

struct node *head;

int main()
{
	head=NULL;
	insert(4);
	insert(3);
	insert(5);
	insert(2);
	insert(8);
	insert(1);
	insert(10);
	insert(10);
	print(head);
	return 0;
}
void print(struct node * head)//print linked list
{
	while(head)
	{
		cout<<head->data<<" ";
		head=head->next;
	}
}

void insert(int data)
{
	struct node* cur=create(data);
	struct node*temp=head;
	if(head==NULL)//if the linked list is empty
	{
		head=cur;
		return;
	}
	else if(head->data>data)//if the new node data is lesser than the data in the header then change the head to new node
	{
		cur->next=head;
		head->prev=cur;
		head=cur;	
	}
	else//find the right position to insert the new node
	{
		
		while(data>=temp->data && temp->next!=NULL)//check for the position
		{
			temp=temp->next;
		}
	
		if(temp->next==NULL)//if the position to insert newnode is at the end of the list
		{		
			temp->next=cur;
			cur->prev=temp;
		}
		else//insert the new node at the correct position
		{
			temp->prev->next=cur;
			cur->prev=temp->prev;
			cur->next=temp;
			temp->prev=cur;
		}	
	}
}
//create a node in the heap
struct node * create(int data)
{
    	struct node * temp= (struct node *) malloc(sizeof(struct node));
	temp->data=data;
	temp->next=NULL;
	temp->prev=NULL;
	return temp;
}
