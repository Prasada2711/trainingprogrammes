#include<bits/stdc++.h>
using namespace std;
#define size 20
class node
{
	public:
		int data;
		node * next;
		node(int data)
		{
			this->data=data;
			this->next=NULL;
		}
	
};
int hashfunction(int key)
{
	return key%size;
}
class linkedlist
{
	public:
	node * head=NULL;
	void insert(int data)
	{
		node * temp=new node(data);	
		if(head==NULL)	//if the list is empty
			head=temp;
		else//insert at begining
		{
			temp->next=head;
			head=temp;
		}
	}
	void delet(int data)
	{
		node * prev=search(data);//return the previous address of node containing the data or returns head if there is only one node 
		if(prev==head)//if there is one node
		{
			free(head);
			return;
		}
		else //change the next field
		{
			node * cur=prev->next;
			prev->next=cur->next;
			free(cur);
		}
			
	}
	node * search(int data)//return the address of previous node to the node containing data
	{
		node * prev=NULL;
		node * cur=head;
		if(data==head->data)
			return head;
		else{
		while(cur && cur->data!=data)
		{
			prev=cur;
			cur=cur->next;		
		}}
		if(cur==NULL){//if the data not found
			cout<<"invalid operation"<<endl;
			return NULL;}
		return prev;		
	}
	void display()
	{
		node *temp=head;
		while(temp)
		{
			cout<<temp->data<<" ";	
			temp=temp->next;	
		}
	}
};
class hashing
{
	public:
	int key;
	linkedlist l;
};

int main()
{
	
	int a[10];
	for(int i=0;i<10;i++)
	{
		a[i]=i+10;
	}
	hashing hasharray[size];
	for(int i=0;i<size;i++)//initialize key values to zero
		hasharray[i].key=0;
	for(int i=0;i<10;i++)
	{
		int hashkey=hashfunction(a[i]);
		if(hasharray[hashkey].key==0)//check for collission
			hasharray[hashkey].key=a[i];
		else
			hasharray[hashkey].l.insert(a[i]);//if there is collision use the linked list
	}
	for(int i=0;i<size;i++)
	{
		hasharray[i].l.display();
		cout<<hasharray[i].key<<" ";
		cout<<endl;	
	}
	return 0;
}
