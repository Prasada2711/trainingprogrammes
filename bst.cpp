#include<bits/stdc++.h>
using namespace std;

struct node
{
	int data;
	struct node * left;
	struct node * right;
};

struct node * insert(struct node * root,int data);
struct node * delet(struct node * root,int data);
struct node * create(int data);
void inorder(struct node * root);
struct node * min(struct node * root);

int main()
{
	struct node * root=NULL;
	root=insert(root,10);
	root=insert(root,20);
	root=insert(root,30);
	root=insert(root,12);
	root=insert(root,13);
	root=insert(root,14);
	inorder(root);
	cout<<endl;
	root=delet(root,12);
	root=delet(root,14);
	inorder(root);
	cout<<endl;
	return 0;
} 

struct node * create(int data)
{
	struct node * temp=(struct node *)malloc(sizeof(struct node));
	temp->data=data;
	temp->left=temp->right=NULL;
	return temp;
}

struct node * insert(struct node * root,int data)
{
	struct node * nnode=create(data);
	if(root==NULL)//if tree is empty
	{	
		root=nnode;	
	}
	else if(data<=root->data)
	{
	root->left=insert(root->left,data);
	}
	else
	root->right=insert(root->right,data);	
}
//inorder traversal 
void inorder(struct node * root)
{
        if(root==NULL)
		return;
	inorder(root->left);
	printf("%d",root->data);
	inorder(root->right);
}
//find the position of the node and delete the node
struct node * delet(struct node * root,int data)
{
	if(root==NULL)//if the tree is empty
		return root;
	
	else if(data<root->data)//if the data is on left side
		root->left=delet(root->left,data);

	else if(data>root->data)//if the data is on right side
		root->right=delet(root->right,data);

	else//node containing the data
	{
		if(root->left==NULL && root->right==NULL)//if the node is leaf node
		{
			delete(root);
			root=NULL;		
		}
		else if(root->left==NULL)//if the node has right child only
		{
			struct node * temp=root;
			root=root->right;
			delete(temp);
		}
		else if(root->right==NULL)//if the node has left child only
		{
			struct node * temp=root;
			root=root->left;
			delete(temp);	
		}	
		else//if the node has both right and left child
		{
			struct node * temp=min(root->right);
			root->data=temp->data;
			root->right=delet(root->right,temp->data);
		}	
					
	}
	return root;
}
//find and return the address of minimum node
struct node * min(struct node * root)
{
	while(root->left)
		root=root->left;
	return root;
}



