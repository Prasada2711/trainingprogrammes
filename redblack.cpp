#include<bits/stdc++.h>
using namespace std;
struct node 
{
	int data;
	struct node * left;
	struct node * right;
	struct node * parent;
	char color;
};

void inorder(struct node * root);
void leftrotate(struct node ** root,struct node *x);
void rightrotate(struct node ** root,struct node *x);
void insert(struct node **root,int data);
void insertfix(struct node ** root,struct node * n);
void validate(struct node * root);
int max(int a,int b){return a>b?a:b;};
int height(struct node * root);

int main()
{
	struct node * root=NULL;
	insert(&root,10);
	insert(&root,20);
	insert(&root,40);
	insert(&root,30);
	insert(&root,50);
	insert(&root,35);
	insert(&root,25);
	insert(&root,37);
	printf("ans is\n");
	inorder(root);
	validate(root);
	return 0;
}
void validate(struct node * root)
{
	int height1=height(root->left);//return the number of nodes with black color on leftside
	int height2=height(root->right);//return the number of nodes with black color on rightside
	int diff=height2-height1;
	if(diff==0)//property of red black tree 
		cout<<"valid";
	else
		cout<<"invaid";
}

int height(struct node * root)
{
	if(root==NULL)
		return 0;
	int left=height(root->left);
	int right=height(root->right);
	if(root->color=='B')
		return max(left,right)+1;//increment the count only if the color is black
	else
		return max(left,right);
}
void inorder(struct node * root)
{
        if(root==NULL)
		return;
	inorder(root->left);
	printf("%d",root->data);
	inorder(root->right);
}

void leftrotate(struct node ** root,struct node *x)
{

	struct node *y=x->right;//point to right child
	x->right=y->left;//assign left child of y to right of x
	if(x->right)
		x->right->parent=x;//assign the parent of right child to x
	if(x->parent==NULL)//if x is the root
		*(root)=y;
	else if(x==x->parent->left)//if x is left child
		x->parent->left=y;
	else//if x is right child
		x->parent->right=y;
	y->parent=x->parent;
	x->parent=y;
	y->left=x;
	
}
void rightrotate(struct node ** root,struct node *x)
{
	struct node * y=x->left;//point to left child
	x->left=y->right;//assign right child of y to left of x
	if(x->left)
		x->left->parent=x;//assign the parent of left child to x
	if(x->parent==NULL)//if x is root
		*(root)=y;
	else if(x==x->parent->right)//if x is right child
		x->parent->right=y;
	else//if x is left child
		x->parent->left=y;
	y->parent=x->parent;
	y->right=x;
	x->parent=y;

}
void insert(struct node **root,int data)
{
	struct node * n=(struct node *)malloc(sizeof(struct node));
	n->data=data;
	n->left = n->right = n->parent = NULL;
	if(*root==NULL)//if tree is empty 
	{
		n->color='B';//root node color is black
		(*root)=n;//make the node as root
	}
	else//find the correct position to insert
	{
		struct node *y=NULL;
		struct node * x=*(root);
		while(x)
		{
			y=x;
			if(x->data<data)//move to right side
				x=x->right;
			else//move to left side
				x=x->left;
			
		}
		n->parent=y;//update parent of newnode 
		if(n->data>y->data)//check newnode as left or right child of y
			y->right=n;
		else
			y->left=n;
	
		n->color='R';//new node colour is red
		insertfix(root,n);
	}

}
void insertfix(struct node ** root,struct node * n)
{	
	while(n!=*root &&n->parent&&n->parent->parent&& n->parent->color=='R')
	{
		struct node * grandfather=n->parent->parent;
		struct node * uncle=NULL;
		if(n->parent==grandfather->left)//find the uncle node
			uncle=grandfather->right;
		else
			uncle=grandfather->left;
		if(uncle && uncle->color=='R')//if uncle color is red
		{
			//change the color of both parent and uncle
			uncle->color='B';
			n->parent->color='B';
			if(grandfather!=*(root))//if grandfather is not root then change the color to red
				grandfather->color='R';
			n=grandfather;
			
		}
		else
		{
			if(n==n->parent->left && n->parent==n->parent->parent->left)//right rotation
			{
				//swap the color of parent node and grandfather
				char ch=n->parent->color;
				n->parent->color=grandfather->color;
				grandfather->color=ch;
				rightrotate(root,grandfather);
			}
			else if(n==n->parent->left && n->parent==n->parent->parent->right)//rl rotation
			{
				//swap the color of current node and grandfather
				char ch=n->color;
				n->color=grandfather->color;
				grandfather->color=ch;				
				rightrotate(root,n->parent);
				leftrotate(root,grandfather);
			}
			else if(n==n->parent->right && n->parent==n->parent->parent->right)//left rotation
			{
				//swap the color of parent node and grandfather
				char ch=n->parent->color;
				n->parent->color=grandfather->color;
				grandfather->color=ch;
				leftrotate(root,grandfather);
			}
			else//lr rotation
			{
				//swap the color of current node and grandfather
				char ch=n->color;
				n->color=grandfather->color;
				grandfather->color=ch;
				leftrotate(root,n->parent);
				rightrotate(root,grandfather);
			}
		}
	}
}

