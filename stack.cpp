#include<iostream>
#define size 10
using namespace std;
class stack
{
	public:
	int a[10];
	int top1=-1;
	void push(int x)
	{
		if(top1==size-1)//overflow condition
			cout<<"overflow"<<endl;
		else
			a[++top1]=x;
	}
	int pop()
	{
		if(top1==-1)//underflow condition
		{
			cout<<"underflow"<<endl;
			return -1;
		}
		else
			return a[top1--];
	}
  	int top()
	{
		return a[top1];
	}
	int isempty()
	{
		if(top1==-1)
			return 1;
		else
			return 0;
	}
	int isfull()
	{
		if(top1==size-1)
			return 1;
		else
			return 0;
	}
};
class queue
{
	public://use 2 stacks
	stack s1;
	stack s2;
	void enqueue(int x)
	{
		if(s1.isfull() || s2.isfull())//overflow condition
			cout<<"overflow"<<endl;
		if(s1.isempty())//checking for selection of stack 
		{
			s2.push(x);		
		}
		else
			s1.push(x);
	}
	int dequeue()
	{
		if(s1.isempty() && s2.isempty())//unserflow condition
			cout<<"underflow"<<endl;
		if(s1.isempty())//if data is present in stack 2
		{
			while(!s2.isempty())//push elements of stack 2 to stack 1
			{
				s1.push(s2.pop());	
			}
			int ans = s1.pop();
			while(!s1.isempty())//push elements of stack 1 to stack 2
			{
				s2.push(s1.pop());			
			}	
			return ans;	
		}
		else//if data is present in stack 1
		{
			while(!s1.isempty())//push elements of stack 1 to stack 2
			{
				s2.push(s1.pop());			
			}			
			int ans=s2.pop();
			while(!s2.isempty())//push elements of stack 2 to stack 1
			{
				s1.push(s2.pop());	
			}
			return ans;
		}
	}
	int isqempty()
	{
		return (s1.isempty() && s2.isempty())?1:0;
	}
};
int main()
{
	queue q;
	q.enqueue(1);
	q.enqueue(2);
	q.enqueue(3);
	q.enqueue(4);
	q.enqueue(5);
	while(!q.isqempty())
	{
	cout<<q.dequeue()<<" ";
	}
	return 0;
}
